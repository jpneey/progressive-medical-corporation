
$(window).scroll(function() {
    counter();    
});

function counter(){
    var hT = $('#counter').offset().top,
        hH = $('#counter').outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
    if (wS > (hT + hH - wH)) {
        $('.counter').each(function() {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2500,
                easing: 'swing',
                step: function(now) {
                    $(this).text(Math.ceil(now));
                }
            });
        }); {
            $('.counter').removeClass('counter').addClass('counted');
        };
    }
}
$(function(){
    $('.modal').modal( {
        endingTop: '15%',
    });

    setTimeout(function(){
        $('#telemodal').modal('open');
    }, 1000)
    $('.carousel-logo').carousel({
        duration: 100,
        dist: 0,
        numVisible: 7,
        noWrap: false
    });
    autoplay();
    function autoplay() {
        $('.carousel-logo').carousel('next');
        setTimeout(autoplay, 2000);
    }  
})